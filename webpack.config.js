const path = require('path');

module.exports = {
    entry: {
        timespent: './src/timespent.js',
        bots: './src/bots.js',
        iframe: './src/iframe.js',
        detectLabel: './src/detectLabel.js',
        combine: './src/combine.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    }
};