import subscribe from 'subscribe-event';
import MobileDetect from 'mobile-detect';

const md = new MobileDetect(window.navigator.userAgent);

let config = {};
let firstEvent = null;
let isBot = true;
let sended = false;

const documentEvents = [
    'touchstart',
    'touchend',
    'touchcancel',
    'touchmove',
    'mousedown',
    'mouseup',
    'keydown',
    'mousemove',
    'wheel',
    'MozMousePixelScroll',
    'mousewheel',
    'click',
];

subscribe(window, 'scroll', handleAction);
documentEvents.forEach((v) => subscribe(document, v, handleAction));


function handleAction() {
    if (!firstEvent) {
        firstEvent = +new Date() / 1000;
        return;
    }
    if (sended) {
        return;
    }
    const now = +new Date() / 1000;
    if (now - firstEvent > config.secondsToWaitEvents) {
        isBot = false;
        sendEvent();
    }
}


function sendEvent() {
    if (!config.trackerUrl || config.trackerUrl.length === 0) {
        return;
    }
    sended = true;
    const o = document.createElement("img");
    o.src = `${config.trackerUrl}click.php?${config.botEvent}=${isBot ? 1 : 0}`;
}


if (md.mobile() || md.phone() || md.tablet()) {
    if (typeof(window.orientation) === "undefined") {
        const o = document.createElement("img");
        o.src = `${config.trackerUrl}click.php?${config.orientationEvent}=1`;
    }
    else {
        const o = document.createElement("img");
        o.src = `${config.trackerUrl}click.php?${config.orientationEvent}=0`;
    }
}

window.detectBots = function (options = {}) {
    config = Object.assign({
        orientationEvent: 'event7',
        botEvent: 'event5',
        trackerUrl: '',
        secondsToWaitEvents: 5
    }, options);
};

subscribe(window, 'beforeunload', function () {
    if (!config.trackerUrl || config.trackerUrl.length === 0) {
        return;
    }
    if (!sended) {
        sendEvent();
    }
});

subscribe(window, 'pagehide', function () {
    if (!config.trackerUrl || config.trackerUrl.length === 0) {
        return;
    }
    if (!sended) {
        sendEvent();
    }
});