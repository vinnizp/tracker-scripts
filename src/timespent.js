import subscribe from 'subscribe-event';

let trackerUrl = '';
let eventName = 'event9';
let debounceTime = 3000;
let timeout;
const startTime = new Date();

const documentEvents = [
    'touchstart',
    'touchend',
    'touchcancel',
    'touchmove',
    'keydown',
    'mousedown',
    'mouseup',
    'mousemove',
    'wheel',
    'MozMousePixelScroll',
    'mousewheel',
    'click',
];

subscribe(window, 'scroll', handleAction);
documentEvents.forEach((v) => subscribe(document, v, handleAction));

function handleAction() {
    if (timeout) {
        clearTimeout(timeout);
    }
    timeout = setTimeout(sendEvent, debounceTime);
}

function sendEvent() {
    if (trackerUrl.length === 0) {
        return;
    }
    const endTime = new Date();
    const time = Math.round((endTime - startTime) / 1000);
    const o = document.createElement("img");
    o.src = `${trackerUrl}click.php?${eventName}=${time}`;
}

subscribe(window, 'beforeunload', function () {
    if (trackerUrl.length === 0) {
        return;
    }
    sendEvent();
});

subscribe(window, 'pagehide', function () {
    if (trackerUrl.length === 0) {
        return;
    }
    sendEvent();
});

window.timespent = function (config) {
    config = Object.assign({
        debounceTime: 3000,
        eventName: 'event9',
        trackerUrl: ''
    }, config);
    if (config.trackerUrl.length === 0) {
        throw new Error('trackerUrl not provided');
    }
    trackerUrl = config.trackerUrl;
    debounceTime = config.debounceTime;
    eventName = config.eventName;
};