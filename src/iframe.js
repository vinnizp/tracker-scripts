import sandblaster from './sandblaster';

let config = {
    eventName: 'event6',
    trackerUrl: ''
};

window.detectSandboxFrame = function (options) {
    config = Object.assign(config, options);
    if(!config.trackerUrl) {
        throw new Error('trackerUrl is required');
    }
    const o = document.createElement("img");
    o.src = `${config.trackerUrl}click.php?${config.eventName}=${sandblaster.unsandbox()?0:1}`;
};