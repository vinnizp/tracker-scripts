import subscribe from 'subscribe-event';

let labels = [
    { selector: '', eventName: '', sended: false }
];

subscribe(window, 'scroll', (e) => {
    if (labels.length > 0) {
        labels = labels.filter((v) => v.sended);
        labels.forEach((v) => {
            if (!v.sended && isInViewport(v.element)) {
                const o = document.createElement("img");
                o.src = `${v.trackerUrl}click.php?${v.eventName}=1`;
            }
        });
    }
});

window.addLabel = function (parameters) {
    let { trackerUrl, selector, eventName } = parameters;
    const element = document.querySelector(selector);
    if (element && element.length > 0) {
        labels.push({ trackerUrl, selector, eventName, element: element[0], sended: false });
    } else {
        throw new Error(`Element '${selector}' Not found`);
    }
};

function isInViewport(element) {
    var rect = element.getBoundingClientRect();
    var html = document.documentElement;
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || html.clientHeight) &&
        rect.right <= (window.innerWidth || html.clientWidth)
    );
}