import './bots';
import './detectLabel';
import './iframe';
import './timespent';

window.detectBots({
    trackerUrl: 'http://in-trafico.tk/',
    orientationEvent: 'event7',
    botEvent: 'event5',
    secondsToWaitEvents: 5
});
var scroll = 0;
window.onscroll = function () {
    if (scroll !== 1) {
        scroll = 1;
        var o = document.createElement("img");
        o.src = "http://in-trafico.tk/click.php?event8=1";
    }
}
window.timespent({
    trackerUrl: 'http://in-trafico.tk/',
    debounceTime: 3000,
    eventName: 'event9'
});
let o;
if (typeof(window.orientation) === "undefined") {
    o = document.createElement("img");
    o.src = 'http://in-trafico.tk/click.php?event10=0';
} else {
    o = document.createElement("img");
    o.src = 'http://in-trafico.tk/click.php?event10=1';
}
